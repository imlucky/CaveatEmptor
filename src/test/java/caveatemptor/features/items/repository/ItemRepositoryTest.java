package caveatemptor.features.items.repository;

import caveatemptor.configs.RootConfig;
import caveatemptor.entities.Category;
import caveatemptor.entities.Item;
import caveatemptor.entities.User;
import caveatemptor.features.items.repositories.ItemRepository;
import caveatemptor.features.items.web.ItemController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("prod")
@ContextConfiguration(classes = RootConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class ItemRepositoryTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ItemRepository itemRepository;

    private Item item;

    @Before
    public void setup() {
        String name = "dotin";
        LocalDateTime auctionEnd = LocalDateTime.now().plusWeeks(1L);
        User user = new User("imlucky", "ali", "dabagh", Collections.emptyList());
        Category category = new Category("Ball");

        BigDecimal initialPrice = new BigDecimal(100);
        item = new Item(name, initialPrice, auctionEnd, category, user);

        em.persist(user);
        em.persist(category);
    }

    @Test
    public void addItemTest() {
        itemRepository.addItem(item);
        Long itemId = item.getId();
        assertThat(itemId, is(notNullValue()));
    }

    @Test
    public void itemListTest() throws Exception {
        ItemController controller = new ItemController();
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/add-item")).andExpect(view().name("item"));
    }


    /*@Test
    public void add_item_test() throws Exception {
        ItemController controller = new ItemController();
        MockMvc mockMvc = standaloneSetup(controller).build();
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String name = ItemRepositoryTest.createStringWithLength(101);
        String initialPrice = ItemRepositoryTest.createStringWithLength(501);
        String auctionEnd = ItemRepositoryTest.createStringWithLength(701);

        mockMvc.perform(
                post("/add-item")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("item-name", name)
                        .param("auction-end", auctionEnd)
                        .param("item-initial-price", initialPrice)
        )
//                .andExpect(status().isOk())
                .andExpect(view().name("/add-item"))
                .andExpect(forwardedUrl("item-list"))

                .andExpect(model().attributeHasFieldErrors("add-item", "item-name"))
                .andExpect(model().attributeHasFieldErrors("add-item", "auction-end"))
                .andExpect(model().attributeHasFieldErrors("add-item", "item-initial-price"))
                .andExpect(model().attribute("add-item", hasProperty("id", nullValue())))
                .andExpect(model().attribute("add-item", hasProperty("description", is(name))))
                .andExpect(model().attribute("add-item", hasProperty("title", is(initialPrice))))
                .andExpect(model().attribute("add-item", hasProperty("title", is(auctionEnd))));
    }

    public static String createStringWithLength(int length) {
        StringBuilder builder = new StringBuilder();

        for (int index = 0; index < length; index++) {
            builder.append("a");
        }

        return builder.toString();
    }*/


    @Test
    public void itemList() {
        itemRepository.getAllItems();
        Long itemId = item.getId();
        assertThat(itemId, is(nullValue()));
    }
}
