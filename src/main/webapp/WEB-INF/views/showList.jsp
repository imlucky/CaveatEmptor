<%--
  Created by IntelliJ IDEA.
  User: dotinschool1
  Date: 8/4/2018
  Time: 4:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link href="/resources/styles/home.css" rel="stylesheet" type="text/css">
</head>
<body>

<div style="margin:auto; height: 500px; width: 40%; overflow: auto; text-align: center">
    <h2>List Of Items</h2>
    <form action="/item-list" method="post">
        <table border="1">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Initial Price</td>
                <td width="35%">Auction End</td>
                <td>Biddable</td>
                <td>Sold</td>
                <td>Bids</td>
            </tr>
            <c:forEach items="${allItems}" var="item">
                <tr>
                    <td><c:out value="${item.id}"/></td>
                    <td><c:out value="${item.name}"/></td>
                    <td><c:out value="${item.initialPrice}"/></td>
                    <td><c:out value="${item.auctionEnd}"/></td>
                    <td><c:out value="${item.biddable}"/></td>
                    <td><c:out value="${item.sold}"/></td>
                    <td><input type="submit" value="Bid"/></td>
                </tr>
            </c:forEach>
        </table>
    </form>
</div>
</body>
</html>
