<%--
  Created by IntelliJ IDEA.
  User: dotinschool1
  Date: 8/6/2018
  Time: 4:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>in bid page</title>
    <link href="/resources/styles/bid.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="select-item">

    <div id="select-item-header">
        <label>Items in: ${parent.name}</label>
    </div>

    <div id="add-bid">
        <form method="post">
            <table>
                <tr>
                    <td>Amount:</td>
                    <td><input type="number" name="amount"><br/></td>

                    <td>CreatedOn:</td>
                    <td><input type="datetime-local" name="created-on"><br/></td>
                    <td><input type="submit" value="Add"><br></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div id="bids">
    <div id="bids-header">
            <label>Bids in: ${parent.name}</label>
    </div>
    <div id="bids-list">

    </div>
</div>
</body>
</html>
