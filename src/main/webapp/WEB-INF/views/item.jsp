<%--
  Created by IntelliJ IDEA.
  User: imlucky
  Date: 8/1/2018
  Time: 9:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Item</title>
    <link href="/resources/styles/home.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="add-item">
    <form action="/add-item" method="post">
        <table id="add-item-table">
            <tr>
                <td>Item Name:</td>
                <td><input type="text" name="item-name"><br/></td>

                <td> Initial price:</td>
                <td><input type="number" name="initial-price"><br/></td>

                <td>Auction End:</td>
                <td><input type="datetime-local" name="auction-end"><br/></td>
                <td><input type="submit" value="Add"><br></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
