@org.hibernate.annotations.GenericGenerator(name = "ID_GENERATOR", strategy = "enhanced-sequence",
        parameters = {
                @org.hibernate.annotations.Parameter(name = "sequence-name", value = "CE_SEQUENCE"),
                @org.hibernate.annotations.Parameter(name = "initial-value", value = "1000")
        }
)
package caveatemptor.entities;


