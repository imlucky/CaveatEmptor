package caveatemptor.entities;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "BD_TYPE")
public abstract class BillingDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = Constants.ID_GENERATOR_NAME)
    private Long id;

    @Column(nullable = false)
    private String owner;

    @ManyToOne(optional = false)
    private User user;

    public BillingDetails() {
    }

    public BillingDetails(String owner, User user) {
        this.owner = owner;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}