package caveatemptor.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class BankAccount extends BillingDetails {

    @Column(nullable = false, updatable = false)
    private String account;

    @Column(nullable = false, updatable = false)
    private String bankName;

    @Column(nullable = false, updatable = false)
    private String swift;

    public BankAccount() {

    }

    public BankAccount(String owner, User user, String account, String bankName, String swift) {
        super(owner, user);
        this.account = account;
        this.bankName = bankName;
        this.swift = swift;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }
}
