package caveatemptor.entities;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "USERS")
public class User {

    @Id
    @GeneratedValue(generator = Constants.ID_GENERATOR_NAME)
    private Long id;

    @Column(length = 16, nullable = false, unique = true)
    private String userName;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<BillingDetails> billingDetails = new ArrayList<>();

    @ElementCollection
    private Set<Address> addresses = new HashSet<>();


    public User(Long id) {
        this.id = id;
    }

    public User() {
    }



    public User(String userName, String firstName, String lastName, List<BillingDetails> billingDetails) {
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.billingDetails = billingDetails;
    }

    public boolean hasAddressOfType(Address.Type type) {
        return addresses.stream().anyMatch(address -> address.getAddressType().equals(type));
    }

    public void addNewAddressOfType(Address address) {
        if (hasAddressOfType(address.getAddressType())) {
            addresses.remove(address);
        }
        addresses.add(address);
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<BillingDetails> getBillingDetails() {
        return billingDetails;
    }

    public void setBillingDetails(List<BillingDetails> billingDetails) {
        this.billingDetails = billingDetails;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(billingDetails, user.billingDetails) &&
                Objects.equals(addresses, user.addresses);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userName, firstName, lastName, billingDetails, addresses);
    }
}
