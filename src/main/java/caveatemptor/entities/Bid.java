package caveatemptor.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class Bid {

    @Id
    @GeneratedValue(generator = Constants.ID_GENERATOR_NAME)
    private Long id;

    @Column(nullable = false)
    private BigDecimal amount;

    @Column(nullable = false)
    private LocalDateTime createdOn;

    @ManyToOne(optional = false)
    private User bidder;

    @ManyToOne(optional = false)
    private Item item;

    public Bid() {
    }

    public Bid(BigDecimal amount, LocalDateTime createdOn, User bidder, Item item) throws Exception {
        this.amount = amount;
        this.createdOn = createdOn;
        this.bidder = bidder;
        if (!item.isBiddable()) {
            throw new Exception("Item is not biddable");
        } else {
            this.item = item;
        }
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public LocalDateTime getcreatedOn() {
        return createdOn;
    }

    public User getBidder() {
        return bidder;
    }

    public Item getItem() {
        return item;
    }
}
