package caveatemptor.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class CreditCard extends BillingDetails {

    @Column(nullable = false, updatable = false, length = 16)
    private String number;

    @Column(nullable = false, updatable = false, length = 2)
    private String expMonth;

    @Column(nullable = false, updatable = false, length = 4)
    private String expYear;

    public CreditCard() {
    }

    public CreditCard(String owner, User user, String number, String expMonth, String expYear) {
        super(owner, user);
        this.number = number;
        this.expMonth = expMonth;
        this.expYear = expYear;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }
}
