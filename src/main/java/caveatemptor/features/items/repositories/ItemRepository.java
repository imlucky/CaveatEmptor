package caveatemptor.features.items.repositories;

import caveatemptor.entities.Item;

import java.util.List;

public interface ItemRepository {

    void addItem(Item item);

    List<Item> getAllItems();
}
