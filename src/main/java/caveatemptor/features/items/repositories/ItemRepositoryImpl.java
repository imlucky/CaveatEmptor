package caveatemptor.features.items.repositories;

import caveatemptor.entities.Item;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ItemRepositoryImpl implements ItemRepository {

    @PersistenceContext
    private EntityManager em;


    @Override
    public void addItem(Item item) {
        em.persist(item);
    }

    @Override
    public List<Item> getAllItems() {
        return em.createQuery("SELECT i from Item i", Item.class).getResultList();
    }
}


