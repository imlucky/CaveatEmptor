package caveatemptor.features.items.vm;

import caveatemptor.entities.Category;
import caveatemptor.entities.Item;
import caveatemptor.entities.User;
import caveatemptor.features.security.DataUtil;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class ItemMapper {


    public Item toEntity(ItemVm itemVm) throws Exception {
        Item item = new Item();

        item.setName(itemVm.getItemName());
        item.setSeller(new User(DataUtil.getCurrentUserId()));
        item.setInitialPrice(itemVm.getInitialPrice());
        item.setAuctionEnd(itemVm.getAuctionEnd());

        Set<Category> cats = new HashSet<>();
        cats.add(new Category(DataUtil.getDefaultCategoryId()));
        item.setCategories(cats);
        return item;
    }
}
