package caveatemptor.features.items.services;

import caveatemptor.entities.Item;
import caveatemptor.features.items.repositories.ItemRepository;
import caveatemptor.features.items.vm.ItemMapper;
import caveatemptor.features.items.vm.ItemVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public void addItem(ItemVm itemVm) throws Exception {
        ItemMapper itemMapper = new ItemMapper();
        Item item = itemMapper.toEntity(itemVm);
        addItem(item);
    }

    @Override
    public void addItem(Item item) {
        itemRepository.addItem(item);
    }

    @Override
    public List<Item> getAllItems() {
        return itemRepository.getAllItems();
    }
}
