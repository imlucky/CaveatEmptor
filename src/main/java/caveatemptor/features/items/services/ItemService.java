package caveatemptor.features.items.services;

import caveatemptor.entities.Item;
import caveatemptor.features.items.vm.ItemVm;

import java.util.List;

public interface ItemService {

    void addItem(ItemVm itemVm) throws Exception;

    void addItem(Item item);

    List<Item> getAllItems();
}
