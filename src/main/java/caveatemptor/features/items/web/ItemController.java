package caveatemptor.features.items.web;

import caveatemptor.entities.Item;
import caveatemptor.features.items.services.ItemService;
import caveatemptor.features.items.vm.ItemVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Controller
@ControllerAdvice
public class ItemController {

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = {"/add-item"}, method = RequestMethod.GET)
    public String items(Model model) {
        //model.addAttribute("parent", null);
        return "item";
    }

    @RequestMapping(value = "/add-item", method = RequestMethod.POST)
    public ModelAndView addItem(
            @RequestParam("item-name") String itemName,
            @RequestParam("auction-end") String auctionEndString,
            @RequestParam("initial-price") String initialPriceString
    ) throws Exception {
        LocalDateTime auctionEnd = LocalDateTime.parse(auctionEndString);
        BigDecimal initialPrice = BigDecimal.valueOf(Double.parseDouble(initialPriceString));

        ItemVm itemVm = new ItemVm();

        itemVm.setItemName(itemName);
        itemVm.setAuctionEnd(auctionEnd);
        itemVm.setInitialPrice(initialPrice);
        itemService.addItem(itemVm);
        return new ModelAndView("redirect:/item-list");
//        return "redirect:/item-list";
        //return "showList";
    }

    /*@RequestMapping(value = "/add-item", method = RequestMethod.POST)
    public ModelAndView addItem(ModelMap model, @ModelAttribute("itemVm") ItemVm itemVm) throws Exception {


        model.addAttribute("itemName", itemVm.getItemName());
        model.addAttribute("initialPrice", itemVm.getInitialPrice());
        model.addAttribute("auctionEnd", itemVm.getAuctionEnd());

        return new ModelAndView("redirect:/item-list");
    }*/


    @RequestMapping(value = "/item-list", method = RequestMethod.GET)
    public String populateItems(Model model) {
        model.addAttribute("allItems", itemService.getAllItems());
        return "showList";
    }
}
